class HelloClass:

    def __init__(self,hello):
        self.hello = hello


    def printHello(self):
        print(self.hello)


if __name__ == "__main__":

    hello = HelloClass("hello")

    hello.printHello()